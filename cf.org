#+TITLE: Moomoo's emacs configuration
#+PROPERTY: header-args :tangle "~/.emacs.d/emacs.el"

* Table Of Contents :TOC_3_gh:
- [[#about-this-file][About This File]]
- [[#emacs-initialization][Emacs Initialization]]
  - [[#garbage-collection][Garbage collection]]
  - [[#links-a-custom-load-path--loads-elisp][Links a custom load-path & loads elisp]]
  - [[#create-customel][Create custom.el]]
- [[#package-management][Package Management]]
  - [[#bootstrapping-use-package][Bootstrapping use-package]]
  - [[#initialize-package-manager][Initialize package manager]]
  - [[#auto-compile-elisp-files][Auto-compile elisp files]]
- [[#aesthetic-customization][Aesthetic Customization]]
  - [[#settings][Settings]]
    - [[#make-emacs-minimal][Make Emacs minimal]]
    - [[#font--frame-size][Font & frame size]]
    - [[#olivetti-for-a-better-environment][Olivetti for a better environment]]
    - [[#set-the-cursor-as-a-vertical-bar][Set the cursor as a vertical bar]]
    - [[#pretty-symbols-for-programming-modes][Pretty symbols for programming modes]]
  - [[#theme][Theme]]
  - [[#mode-line][Mode-line]]
- [[#emacs-completion][Emacs Completion]]
  - [[#ido-mode][Ido-mode]]
  - [[#amx][Amx]]
  - [[#which-key][Which-key]]
- [[#general-configuration][General Configuration]]
  - [[#window-management][Window management]]
    - [[#winner-mode][Winner mode]]
    - [[#hydras][Hydra(s)]]
  - [[#dired][Dired]]
    - [[#enable-dired-x][Enable dired-x]]
    - [[#sort-directories-first][Sort directories first]]
    - [[#compressuncompress-non-tar-files][Compress/uncompress non-tar files]]
    - [[#one-confirmation-for-all-selected-files][One confirmation for all selected files]]
    - [[#follow-symlinked-directories][Follow symlinked directories]]
    - [[#try-suggesting-dired-targets][Try suggesting dired targets]]
    - [[#visit-home-directory-whenever][Visit home directory whenever]]
    - [[#dired-hackss-additions][Dired-hacks's additions]]
  - [[#ibuffer][Ibuffer]]
    - [[#configuration][Configuration]]
    - [[#define-filter-groups][Define filter groups]]
    - [[#hydras-1][Hydra(s)]]
  - [[#org][Org]]
    - [[#configuration-1][Configuration]]
    - [[#org-agenda][Org-agenda]]
    - [[#org-capture][Org-capture]]
    - [[#bullets][Bullets]]
    - [[#toc-org][Toc-org]]
  - [[#gnus][Gnus]]
  - [[#version-control][Version control]]
  - [[#backup-files][Backup files]]
  - [[#refresh-buffers][Refresh buffers]]
  - [[#text][Text]]
  - [[#tabs][Tabs]]
  - [[#yes-and-no][Yes and no]]
  - [[#undoredo][Undo/Redo]]
  - [[#parens][Parens]]
  - [[#keystrokes][Keystrokes]]
  - [[#lines][Lines]]
  - [[#buffers][Buffers]]
  - [[#manpages][Manpages]]
- [[#hydra][Hydra]]
    - [[#package-settings][Package settings]]
    - [[#window-management-1][Window management]]
    - [[#mini-vi][Mini-vi]]
- [[#yasnippet][Yasnippet]]
- [[#writing][Writing]]
  - [[#text-mode-hook][Text-mode hook]]
  - [[#dictionary][Dictionary]]
  - [[#spellcheck][Spellcheck]]
    - [[#define-setq][Define Setq]]
    - [[#correction-interface][Correction interface]]
    - [[#switch-between-languages][Switch between languages]]
- [[#programming][Programming]]
  - [[#prog-mode-hook][Prog-mode hook]]
  - [[#packages][Packages]]
    - [[#rainbow-mode][Rainbow mode]]
    - [[#rainbow-delimiters][Rainbow delimiters]]
  - [[#elisp-editing][Elisp editing]]
    - [[#elisp-mode-hook][Elisp-mode hook]]
  - [[#lua-editing][Lua editing]]
    - [[#add-lua-mode-to-emacs][Add lua-mode to emacs]]
    - [[#lua-mode-hook][Lua-mode hook]]

* About This File
This is my personal(& literate) Emacs configuration file, which will be tangled and have its code blocks read through org-babel! Ain't that nice? This file is sourced inside init.el and wil be compiled whenever a change happens. I try to optimize loading time as much as I know - though it won't matter much to me, as I never get out of Emacs.

* Emacs Initialization
Things Emacs should create and check at startup. 
** Garbage collection
Let's first disable =gc-cons-threshold= for the initialization and re-enable it through an =after-init-hook=. This reduces =package-initialize= time for about half.
#+BEGIN_SRC emacs-lisp
;;; -*- lexical-binding: t -*-
(setq gc-cons-threshold 64000000)
(add-hook 'after-init-hook #'(lambda ()
          (setq gc-cons-threshold 800000)))
#+END_SRC

** Links a custom load-path & loads elisp
If I ever load elisp outside of the package archives, this is where Emacs will find them. This is also good in case I need to load files that contains private information.
#+BEGIN_SRC emacs-lisp
(add-to-list 'load-path "~/.emacs.d/elisp/")
(load "ido-grid")
#+END_SRC

** Create custom.el
So =init.el= won't get cluttered with customization changes. 
#+BEGIN_SRC emacs-lisp
(setq-default custom-file (expand-file-name ".custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
(load custom-file))
#+END_SRC

* Package Management
Love me some sweet, sweet packages!
** Bootstrapping use-package
First, let's add package archives to =package.el= and specify the settings. By putting =package-enable-at-startup= to =nil=, the packages will not be automatically loaded - which is good, because this is =use-package='s job. =load-prefer-newer= is enabled so Emacs will always prefer new byte code. At last, we call =use-package=.
#+BEGIN_SRC emacs-lisp
(setq package-archives
      '(("gnu" . "https://elpa.gnu.org/packages/")
        ("org" . "http://orgmode.org/elpa/")
        ("melpa" . "https://melpa.org/packages/")
        ("melpa-stable" . "https://stable.melpa.org/packages/"))
      package-user-dir "~/.emacs.d/elpa/"
      package-enable-at-startup nil
      inhibit-default-init t
      load-prefer-newer t)
(package-initialize 'noactivate)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
(package-install 'use-package))
#+END_SRC

** Initialize package manager
Now, we'll start package management! =use-package= is set to always defer & ensure, making sure packages will only be loaded when they are used and automatically install any packages if not already present.
#+BEGIN_SRC emacs-lisp
(package-initialize)
(eval-when-compile
  (require 'use-package)
(setq use-package-verbose t
      use-package-always-ensure t
      use-package-always-defer t))
#+END_SRC

** Auto-compile elisp files
Guarantees that Emacs never loads outdated byte code files. Putting it there to make it work as early as possible.
#+BEGIN_SRC emacs-lisp 
(use-package auto-compile
  :demand t
  :config
  (auto-compile-on-load-mode)
  (auto-compile-on-save-mode)
  :init (setq auto-compile-display-buffer nil
              auto-compile-mode-line-counter t
              auto-compile-source-recreate-deletes-dest t
              auto-compile-toggle-deletes-nonlib-dest t
              auto-compile-update-autoloads t))
#+END_SRC

* Aesthetic Customization
Because I like it when my text editor/lisp interpreter looks pretty.
** Settings
Choices for a more pleasant look.
*** Make Emacs minimal
#+BEGIN_SRC emacs-lisp
(scroll-bar-mode -1)
(tool-bar-mode   -1)
(tooltip-mode    -1)
(menu-bar-mode   -1)
(blink-cursor-mode 0)
(fringe-mode 0)
(setq ring-bell-function 'ignore
      scroll-conservatively 100)
#+END_SRC

*** Font & frame size
#+BEGIN_SRC emacs-lisp 
(add-to-list 'default-frame-alist '(font . "noto sans mono 11"))
(add-to-list 'default-frame-alist '(height . 30))
(add-to-list 'default-frame-alist '(width . 90))
#+END_SRC

*** Olivetti for a better environment
#+BEGIN_SRC emacs-lisp 
(use-package olivetti
  :hook ((prog-mode text-mode conf-mode) . olivetti-mode))
#+END_SRC

*** Set the cursor as a vertical bar
#+BEGIN_SRC emacs-lisp
(setq-default cursor-type 'bar)
#+END_SRC

*** Pretty symbols for programming modes
#+BEGIN_SRC emacs-lisp 
(global-prettify-symbols-mode 1)
(setq prettify-symbols-alist
      '(
        ("lambda" . 955) ; λ
        ("->" . 8594)    ; →
        ("=>" . 8658)    ; ⇒
        ("map" . 8614)    ; ↦
        ))
#+END_SRC

** Theme
Like its github page says: A customizable colorful eye-candy theme for Emacser. Moe, moe, kyun! Let's also write some mode-line settings to play nice with a certain package.
#+BEGIN_SRC emacs-lisp  
(use-package moe-theme
  :demand t
  :init
  (setq moe-theme-highlight-buffer-id t
        moe-theme-resize-org-title '(1.4 1.3 1.3 1.2 1.1 1.0 1.0 1.0 1.0)
        moe-theme-resize-rst-title '(1.4 1.3 1.2 1.2 1.1 1.0))
  :config
  (let ((line (face-attribute 'mode-line :underline)))
    (set-face-attribute 'mode-line          nil :overline   line)
    (set-face-attribute 'mode-line-inactive nil :overline   line)
    (set-face-attribute 'mode-line-inactive nil :underline  line)
    (set-face-attribute 'mode-line          nil :box        nil)
    (set-face-attribute 'mode-line-inactive nil :box        nil)
    (set-face-attribute 'mode-line-inactive nil :background "#005f87"))

(moe-theme-set-color 'red)
(moe-dark))
#+END_SRC

** Mode-line
Moody for a prettier modeline, nyan mode because...nyan cat is cute! and minions for space. My modeline is quite vanilla if you ignore these packages and a few enabled settings.
#+BEGIN_SRC emacs-lisp
(use-package minions)
(use-package moody
  :demand t
  :init
  (setq x-underline-at-descent-line t
        minions-mode-line-lighter "❤")
  :config
  (line-number-mode 1)
  (column-number-mode 1)
  (size-indication-mode 1)
  (minions-mode 1)  
  (moody-replace-mode-line-buffer-identification)
  (moody-replace-vc-mode))
#+END_SRC

* Emacs Completion
Make it suggest things to you.
** Ido-mode
I dunno about helm, and ivy didn't interest me outside of swiper; but I can live without it. This leaves me with the built-in ido, which isn't bad nor hard to improve. We can do this by first enabling =ido-mode= and make =ido-everywhere= behave how it should. 
#+BEGIN_SRC emacs-lisp
(use-package ido-completing-read+
  :demand t
  :init (setq ido-enable-flex-matching t
              ido-grid-max-columns nil
              ido-case-fold t)
  :config (ido-mode 1)
          (ido-everywhere 1)
          (ido-ubiquitous-mode 1)
          (ido-grid-enable))
#+END_SRC

** Amx
And add M-x completion with enhancements! Along with ido's autocompletion feature, amx will memorize your most used commands and lists them first.
#+BEGIN_SRC emacs-lisp
(use-package amx
  :demand t
  :init (setq amx-ignored-command-matchers nil
              amx-show-key-bindings nil)
  :config (amx-initialize)
  :bind (("M-x" . amx)
        ("M-X" . amx-major-mode-commands)
        ("C-c C-c M-x" . execute-extended-command)))
#+END_SRC

** Which-key
Displays avaliable keybindings in popup. Useful if you don't have a good memory, like me.
#+BEGIN_SRC emacs-lisp
(use-package which-key
  :demand t
  :init (setq which-key-side-window-location 'bottom
              which-key-side-window-max-width 0.33
              which-key-side-window-max-height 0.25
              which-key-sort-order 'which-key-key-order-alpha
              which-key-idle-delay 0.35)
  :config (which-key-mode))
#+END_SRC

* General Configuration
** Window management
*** Winner mode
#+BEGIN_SRC emacs-lisp
(winner-mode 1)
#+END_SRC

*** Hydra(s)
#+BEGIN_SRC emacs-lisp 
;(global-set-key (kbd "C-c w") 'hydra-window/body)
(defhydra hydra-window (:color red
                        :hint nil)
  "
 Split: _v_ert _x_:horz
Delete: _o_nly  _da_ce  _dw_indow  _db_uffer  _df_rame
  Move: _s_wap
Frames: _f_rame new  _df_ delete
  Misc: _m_ark _a_ce  _u_ndo  _r_edo _q_uit"
  ("h" windmove-left)
  ("j" windmove-down)
  ("k" windmove-up)
  ("l" windmove-right)
  ("H" hydra-move-splitter-left)
  ("J" hydra-move-splitter-down)
  ("K" hydra-move-splitter-up)
  ("L" hydra-move-splitter-right)
  ("|" (lambda ()
         (interactive)
         (split-window-right)
         (windmove-right)))
  ("_" (lambda ()
         (interactive)
         (split-window-below)
         (windmove-down)))
  ("v" split-window-right)
  ("x" split-window-below)
  ;("t" transpose-frame "'")
  ;; winner-mode must be enabled
  ("u" winner-undo)
  ("r" winner-redo) ;;Fixme, not working?
  ("o" delete-other-windows :exit t)
  ("a" ace-window :exit t)
  ("f" new-frame :exit t)
  ("s" ace-swap-window)
  ("da" ace-delete-window)
  ("dw" delete-window)
  ("db" kill-this-buffer)
  ("df" delete-frame :exit t)
  ("q" nil)
  ;("i" ace-maximize-window "ace-one" :color blue)
  ;("b" ido-switch-buffer "buf")
  ("m" headlong-bookmark-jump))
#+END_SRC

** Dired
Emacs's file manager.
*** Enable dired-x
#+BEGIN_SRC emacs-lisp
(add-hook 'dired-load-hook
            (function (lambda () (load "dired-x"))))
#+END_SRC

*** Sort directories first
#+BEGIN_SRC emacs-lisp
(setq dired-listing-switches "-aBhl --group-directories-first")
#+END_SRC

*** Compress/uncompress non-tar files
#+BEGIN_SRC emacs-lisp
(eval-after-load "dired-aux"
   '(add-to-list 'dired-compress-file-suffixes 
                 '("\\.zip\\'" ".zip" "unzip" "\\.rar\\'" ".rar" "unrar")))
#+END_SRC

*** One confirmation for all selected files
#+BEGIN_SRC emacs-lisp
(setq dired-recursive-deletes 'always
      dired-recursive-copies 'always)
#+END_SRC

*** Follow symlinked directories
#+BEGIN_SRC emacs-lisp
(setq find-file-visit-truename t)
#+END_SRC

*** Try suggesting dired targets
#+BEGIN_SRC emacs-lisp
(setq dired-dwim-target t)
#+END_SRC

*** Visit home directory whenever
#+BEGIN_SRC emacs-lisp
(global-set-key (kbd "S-<f1>")
  (lambda ()
    (interactive)
    (dired "~/")))
#+END_SRC

*** Dired-hacks's additions
Nifty extra functionality to pump dired.
**** Dired-filter
#+BEGIN_SRC emacs-lisp
(use-package dired-filter
  :after dired
  :init (setq dired-filter-group-saved-groups
        '(("default"
        ("Places" (directory))
        ("Documents"
          (extension "org" "doc" "docx" "odt" "pdb" "epub" "pdf"))
        ("Fonts"
          (extension "tff" "pcf" "bdf"))
        ("Archives"
          (extension "zip" "tgz" "gz" "xz" "jar" "7z" "rar" "sar" "xpi" "apk" "tar"))
        ("Images"
          (extension "jpg" "png" "jpeg" "gif" "bmp" "svg"))
        ("Media"
          (extension "mp3" "mp4" "avi" "mpg" "flv" "ogg"))
           )))
  :hook (dired-mode . dired-filter-group-mode))
#+END_SRC

**** Dired-subtree
Inserts a subdirectory below its line in a tree-like structure.
#+BEGIN_SRC emacs-lisp
(use-package dired-subtree
  :after dired
  :bind (:map dired-mode-map
        ("i" . dired-subtree-insert)
        (";" . dired-subtree-remove)))
#+END_SRC

** Ibuffer
Dired-like buffer management.
*** Configuration
**** Replace buffer-menu with ibuffer
#+BEGIN_SRC emacs-lisp
(global-set-key (kbd "C-x C-b") 'ibuffer-other-window)
#+END_SRC

**** Do not show empty filter groups
#+BEGIN_SRC emacs-lisp
(setq ibuffer-show-empty-filter-groups nil)
#+END_SRC

**** Keep list up-to-date
#+BEGIN_SRC emacs-lisp
(add-hook 'ibuffer-mode-hook
	  '(lambda ()
	  (ibuffer-auto-mode 1)
	  (ibuffer-switch-to-saved-filter-groups "home")))
#+END_SRC

*** Define filter groups
#+BEGIN_SRC emacs-lisp
(setq ibuffer-saved-filter-groups
      '(("home"
      ("Dired" (mode . dired-mode))
      ("Erc" (mode . erc-mode))
      ("Org" (mode . org-mode))
      ("Emacs" (or
       (name . "^\\*scratch\\*$")
       (name . "^\\*Messages\\*$")
       (filename . "cf.org")
       (filename . "init.el")))
      ("Programming" (or
       (mode . c-mode)
       (mode . lua-mode)
       (mode . perl-mode)
       (mode . python-mode)
       (mode . emacs-lisp-mode)))
      ("Gnus" (or
       (mode . message-mode)
       (mode . bbdb-mode)
       (mode . mail-mode)
       (mode . gnus-group-mode)
       (mode . gnus-summary-mode)
       (mode . gnus-article-mode)
       (name . "^\\.bbdb$")
       (name . "^\\.newsrc-dribble"))))))
#+END_SRC

*** Hydra(s)
#+BEGIN_SRC emacs-lisp 
(with-eval-after-load 'ibuffer 
  (define-key ibuffer-mode-map "." 'hydra-ibuffer-main/body))

(defhydra hydra-ibuffer-main (:color pink :hint nil)
  "
 ^Navigation^ | ^Mark^        | ^Actions^        | ^View^
-^----------^-+-^----^--------+-^-------^--------+-^----^-------
  _k_:    ʌ   | _m_: mark     | _D_: delete      | _g_: refresh
 _RET_: visit | _u_: unmark   | _S_: save        | _s_: sort
  _j_:    v   | _*_: specific | _a_: all actions | _/_: filter
-^----------^-+-^----^--------+-^-------^--------+-^----^-------
"
  ("j" ibuffer-forward-line)
  ("RET" ibuffer-visit-buffer :color blue)
  ("k" ibuffer-backward-line)

  ("m" ibuffer-mark-forward)
  ("u" ibuffer-unmark-forward)
  ("*" hydra-ibuffer-mark/body :color blue)

  ("D" ibuffer-do-delete)
  ("S" ibuffer-do-save)
  ("a" hydra-ibuffer-action/body :color blue)

  ("g" ibuffer-update)
  ("s" hydra-ibuffer-sort/body :color blue)
  ("/" hydra-ibuffer-filter/body :color blue)

  ("o" ibuffer-visit-buffer-other-window "other window" :color blue)
  ("q" quit-window "quit ibuffer" :color blue)
  ("." nil "toggle hydra" :color blue))

(defhydra hydra-ibuffer-mark (:color teal :columns 5
                              :after-exit (hydra-ibuffer-main/body))
  "Mark"
  ("*" ibuffer-unmark-all "unmark all")
  ("M" ibuffer-mark-by-mode "mode")
  ("m" ibuffer-mark-modified-buffers "modified")
  ("u" ibuffer-mark-unsaved-buffers "unsaved")
  ("s" ibuffer-mark-special-buffers "special")
  ("r" ibuffer-mark-read-only-buffers "read-only")
  ("/" ibuffer-mark-dired-buffers "dired")
  ("e" ibuffer-mark-dissociated-buffers "dissociated")
  ("h" ibuffer-mark-help-buffers "help")
  ("z" ibuffer-mark-compressed-file-buffers "compressed")
  ("b" hydra-ibuffer-main/body "back" :color blue))

(defhydra hydra-ibuffer-action (:color teal :columns 4
                                :after-exit
                                (if (eq major-mode 'ibuffer-mode)
                                    (hydra-ibuffer-main/body)))
  "Action"
  ("A" ibuffer-do-view "view")
  ("E" ibuffer-do-eval "eval")
  ("F" ibuffer-do-shell-command-file "shell-command-file")
  ("I" ibuffer-do-query-replace-regexp "query-replace-regexp")
  ("H" ibuffer-do-view-other-frame "view-other-frame")
  ("N" ibuffer-do-shell-command-pipe-replace "shell-cmd-pipe-replace")
  ("M" ibuffer-do-toggle-modified "toggle-modified")
  ("O" ibuffer-do-occur "occur")
  ("P" ibuffer-do-print "print")
  ("Q" ibuffer-do-query-replace "query-replace")
  ("R" ibuffer-do-rename-uniquely "rename-uniquely")
  ("T" ibuffer-do-toggle-read-only "toggle-read-only")
  ("U" ibuffer-do-replace-regexp "replace-regexp")
  ("V" ibuffer-do-revert "revert")
  ("W" ibuffer-do-view-and-eval "view-and-eval")
  ("X" ibuffer-do-shell-command-pipe "shell-command-pipe")
  ("b" nil "back"))

(defhydra hydra-ibuffer-sort (:color amaranth :columns 3)
  "Sort"
  ("i" ibuffer-invert-sorting "invert")
  ("a" ibuffer-do-sort-by-alphabetic "alphabetic")
  ("v" ibuffer-do-sort-by-recency "recently used")
  ("s" ibuffer-do-sort-by-size "size")
  ("f" ibuffer-do-sort-by-filename/process "filename")
  ("m" ibuffer-do-sort-by-major-mode "mode")
  ("b" hydra-ibuffer-main/body "back" :color blue))

(defhydra hydra-ibuffer-filter (:color amaranth :columns 4)
  "Filter"
  ("m" ibuffer-filter-by-used-mode "mode")
  ("M" ibuffer-filter-by-derived-mode "derived mode")
  ("n" ibuffer-filter-by-name "name")
  ("c" ibuffer-filter-by-content "content")
  ("e" ibuffer-filter-by-predicate "predicate")
  ("f" ibuffer-filter-by-filename "filename")
  (">" ibuffer-filter-by-size-gt "size")
  ("<" ibuffer-filter-by-size-lt "size")
  ("/" ibuffer-filter-disable "disable")
  ("b" hydra-ibuffer-main/body "back" :color blue))
#+END_SRC

** Org
Kitchen sink inside a kitchen sink.
*** Configuration
**** Define Setq
Various quality of life improvements.
#+BEGIN_SRC emacs-lisp
(setq org-pretty-entities t
      org-ellipsis " ⤵"
      org-src-fontify-natively t
      org-src-tab-acts-natively t
      org-src-window-setup 'current-window
      org-fontify-whole-heading-line t
      org-fontify-done-headline t
      org-fontify-quote-and-verse-blocks t
      org-highlight-latex-and-related '(latex)
      org-adapt-indentation nil
      org-log-done 'time
      org-enforce-todo-dependencies t
      org-completion-use-ido t
      org-list-description-max-indent 5)
#+END_SRC

**** Do not let Ispell correct SRC blocks
It's really annoying when this happens.
#+BEGIN_SRC emacs-lisp
(add-to-list 'ispell-skip-region-alist '("#\\+BEGIN_SRC" . "#\\+END_SRC"))
(add-to-list 'ispell-skip-region-alist '("#\\+BEGIN_LATEX" . "#\\+END_LATEX"))
#+END_SRC

**** TODO(s) keywords
Useful set of keywords.
#+BEGIN_SRC emacs-lisp
(setq org-todo-keywords
  '((sequence "☛ TODO" "✎ IN-PROGRESS" "⚑ WAITING" "✓ DONE" "✘ CANCELLED"))
      org-todo-keyword-faces
  '(("☛ TODO" . org-warning) ("✎ IN-PROGRESS" . "orange") ("✓ DONE" . "green")
      ("⚑ WAITING" . "yellow") ("✘ CANCELED" . "red")))
#+END_SRC

*** Org-agenda
**** Keybindings
#+BEGIN_SRC emacs-lisp :tangle no
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
#+END_SRC

**** Settings
#+BEGIN_SRC emacs-lisp :noexport:
(setq org-agenda-use-time-grid nil
      org-agenda-skip-deadline-if-done t
      org-agenda-skip-scheduled-if-done t
      org-agenda-files (list "~/Documentos/Org/Agenda.org"))
#+END_SRC

*** Org-capture
**** Set target file for notes
#+BEGIN_SRC emacs-lisp
(setq org-default-notes-file (concat org-directory "~/Documentos/Org/Captures.org"))
#+END_SRC

**** Capture templates
#+BEGIN_SRC emacs-lisp :tangle no
#+END_SRC

*** Bullets
Pretty utf-8 bullets for org-mode.
#+BEGIN_SRC emacs-lisp
(use-package org-bullets
  :after org
  :init (setq org-bullets-bullet-list '("➤" "◉" "♦" "✿"))
  :hook (org-mode . org-bullets-mode))
#+END_SRC

*** Toc-org
Org-mode package that generates an up to date table of contents.
#+BEGIN_SRC emacs-lisp
(use-package toc-org
  :after org
  :hook (org-mode . toc-org-enable))
#+END_SRC

** Gnus
** Version control
** Backup files
Stop creating backup~ and #autosave# files. I don't care if disk space is cheap.
#+BEGIN_SRC emacs-lisp
(setq make-backup-files nil
      auto-save-default nil)
#+END_SRC

** Refresh buffers
Auto refresh buffers.
#+BEGIN_SRC emacs-lisp
(global-auto-revert-mode 1)
#+END_SRC

Also dired buffers, but be quiet about it.
#+BEGIN_SRC emacs-lisp
(setq global-auto-revert-non-file-buffers t
      auto-revert-verbose nil)
#+END_SRC

** Text
Sane use of clipboard selection, active region, removal of selected text and word-wrapping.
#+BEGIN_SRC emacs-lisp
(delete-selection-mode t)
(transient-mark-mode t)
(setq x-select-enable-clipboard t
      word-wrap t)
#+END_SRC

** Tabs
Define tab size and indentation.
#+BEGIN_SRC emacs-lisp
(setq tab-width 2
      indent-tabs-mode nil)
#+END_SRC

** Yes and no
Shorter yes and no.
#+BEGIN_SRC emacs-lisp
(defalias 'yes-or-no-p 'y-or-n-p)
#+END_SRC

** Undo/Redo
#+BEGIN_SRC emacs-lisp :tangle no
(use-package undo-propose)
#+END_SRC

** Parens
Matching parens!
#+BEGIN_SRC emacs-lisp
(setq show-paren-delay 0
      show-paren-style 'expression)
(show-paren-mode 1)
#+END_SRC

** Keystrokes
Show keystrokes in progress.
#+BEGIN_SRC emacs-lisp
(setq echo-keystrokes 0.1)
#+END_SRC

** Lines
Lines with 80 characters wide.
#+BEGIN_SRC emacs-lisp
(setq fill-column 80)
(set-default 'fill-column 80)
#+END_SRC

** Buffers
Disable startup screen, set major mode for =*scratch*= and define message on it.
#+BEGIN_SRC emacs-lisp
(setq inhibit-startup-screen t
      initial-major-mode 'org-mode
      initial-scratch-message "* this is an org-mode scratch buffer 
 - type what you want here...")
#+END_SRC

** Manpages
Change disturbing manpage behavior.
#+BEGIN_SRC emacs-lisp
(with-eval-after-load "man" 
  (progn
    (setq Man-notify-method 'pushy)
  )
)
#+END_SRC

* Hydra
A package to create keybindings that sticks around; aka, tie related commands into a group of short bindings with a common prefix.
*** Package settings
#+BEGIN_SRC emacs-lisp 
(use-package hydra
  :init (setq hydra-verbose t
              hydra-is-helpful t
              hydra-lv t
              lv-use-separator t))
#+END_SRC

*** Window management
#+BEGIN_SRC emacs-lisp :tangle no
(global-set-key (kbd "C-c w") 'hydra-window/body)
(defhydra hydra-window (:color red
                        :hint nil)
  "
 Split: _v_ert _x_:horz
Delete: _o_nly  _da_ce  _dw_indow  _db_uffer  _df_rame
  Move: _s_wap
Frames: _f_rame new  _df_ delete
  Misc: _m_ark _a_ce  _u_ndo  _r_edo"
  ("h" windmove-left)
  ("j" windmove-down)
  ("k" windmove-up)
  ("l" windmove-right)
  ("H" hydra-move-splitter-left)
  ("J" hydra-move-splitter-down)
  ("K" hydra-move-splitter-up)
  ("L" hydra-move-splitter-right)
  ("|" (lambda ()
         (interactive)
         (split-window-right)
         (windmove-right)))
  ("_" (lambda ()
         (interactive)
         (split-window-below)
         (windmove-down)))
  ("v" split-window-right)
  ("x" split-window-below)
  ;("t" transpose-frame "'")
  ;; winner-mode must be enabled
  ("u" winner-undo)
  ("r" winner-redo) ;;Fixme, not working?
  ("o" delete-other-windows :exit t)
  ("a" ace-window :exit t)
  ("f" new-frame :exit t)
  ("s" ace-swap-window)
  ("da" ace-delete-window)
  ("dw" delete-window)
  ("db" kill-this-buffer)
  ("df" delete-frame :exit t)
  ("q" nil)
  ;("i" ace-maximize-window "ace-one" :color blue)
  ;("b" ido-switch-buffer "buf")
  ("m" headlong-bookmark-jump))
#+END_SRC

*** Mini-vi
#+BEGIN_SRC emacs-lisp :tangle no
(defun hydra-vi/pre ()
  (set-cursor-color "#e52b50"))

(defun hydra-vi/post ()
  (set-cursor-color "#ffffff"))

   (defhydra hydra-vi (:pre hydra-vi/pre :post hydra-vi/post :color amaranth)
     "vi"
     ("l" forward-char)
     ("h" backward-char)
     ("j" next-line)
     ("k" previous-line)
     ("m" set-mark-command "mark")
     ("a" move-beginning-of-line "beg")
     ("e" move-end-of-line "end")
     ("d" delete-region "del" :color blue)
     ("y" kill-ring-save "yank" :color blue)
     ("q" nil "quit"))
(hydra-set-property 'hydra-vi :verbosity 1)
#+END_SRC

* Yasnippet
* Writing
** Text-mode hook
Add a hook that enables built-in minor modes on text-mode.
#+BEGIN_SRC emacs-lisp :tangle no
(add-hook 'text-mode-hook
    (visual-line-mode t) (flyspell-mode -1) )
#+END_SRC

** Dictionary
** Spellcheck
Spellchecking with flyspell / hunspell.
*** Define Setq
#+BEGIN_SRC emacs-lisp :tangle no
(setq flyspell-issue-message-flag nil
      ispell-program-name "hunspell"
      ispell-dictionary "en_US")
#+END_SRC

*** Correction interface
#+BEGIN_SRC emacs-lisp :tangle no
(use-package flyspell-correct
  :bind ("C-M-;" . flyspell-correct-wrapper)
  :init
  (setq flyspell-correct-interface #'flyspell-correct-ido))
#+END_SRC

*** Switch between languages
#+BEGIN_SRC emacs-lisp :tangle no
(let ((langs '("en_US" "pt_BR")))
      (setq lang-ring (make-ring (length langs)))
      (dolist (elem langs) (ring-insert lang-ring elem)))

(defun cycle-ispell-languages ()
      (interactive)
      (let ((lang (ring-ref lang-ring -1)))
        (ring-insert lang-ring lang)
        (ispell-change-dictionary lang)))

(global-set-key [f6] 'cycle-ispell-languages)
#+END_SRC

* Programming
** Prog-mode hook
Add a hook that enables built-in minor modes on prog-mode.
#+BEGIN_SRC emacs-lisp 
(add-hook 'prog-mode-hook 
    (visual-line-mode t) (electric-pair-mode 1))
#+END_SRC

** Packages
For more than one mode.
*** Rainbow mode
Colorize color names in buffers.
#+BEGIN_SRC emacs-lisp 
(use-package rainbow-mode
  :hook (prog-mode conf-mode))
#+END_SRC

*** Rainbow delimiters
#+BEGIN_SRC emacs-lisp 
(use-package rainbow-delimiters
  :hook ((emacs-lisp-mode lua-mode) . rainbow-delimiters-mode))
#+END_SRC

** Elisp editing
*** Elisp-mode hook
** Lua editing
*** Add lua-mode to emacs
#+BEGIN_SRC emacs-lisp 
(use-package lua-mode
  :mode (("\\.lua\\'" . lua-mode))
  :interpreter ("lua" . lua-mode))
#+END_SRC

*** Lua-mode hook
