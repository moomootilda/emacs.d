;;; -*- lexical-binding: t -*-

;; don't attempt to find/apply special file handlers to files loaded during startup.
(let ((file-name-handler-alist nil))
  
 ;; if config is pre-compiled, then load that
(if (file-exists-p (expand-file-name "emacs.el" user-emacs-directory))
    (load-file (expand-file-name "emacs.el" user-emacs-directory))
  
;; otherwise use org-babel to tangle and load the configuration
(org-babel-load-file (expand-file-name "cf.org" user-emacs-directory))))

(garbage-collect)
(put 'dired-find-alternate-file 'disabled nil)
